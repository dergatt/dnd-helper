# DnD helper

This is a helper program for DnD Dungeon masters. It helps to keep track of players and enemys and thier turn order.

You can create chracters (players and enemys) and assign the nesessary values. When a enemy is created the initative is randomly generated and the dex modiefier is added.


## run the program
To run this program just download the source code [here](https://gitlab.com/dergatt/dnd-helper/-/archive/master/dnd-helper-master.zip). Extract it and open the main-site.html in your browser. A internet connection is required as assets need to be loaded from external servers.
