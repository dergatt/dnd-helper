
var enemys = [];
var players = [];
var all = [];
var allSorted = [];
var turn = 0;
var currentTurn = 0;

function createUid () {
	let uidValue = Math.random().toString(36).substr(2);
	while (typeof search(uidValue) === "number") {
		uidValue = Math.random().toString(36).substr(2);
	}
	return uidValue;
}

//Object Consturctor
//creates an enemy character
function createEnemy() {
	var inputname = document.getElementById("nameEnemy").value;
	var inputHp = document.getElementById("hpEnemy").value;
	var inputMod = Number(document.getElementById("initativeEnemy").value);
	var inputAmmount = Number(document.getElementById("numberEnemy").value);
	console.log("How many: " + inputAmmount);

	for (let i = 0; i < inputAmmount; i++) {
		console.log("I is " + i);
		//Creates uid for character and checks if the same uid allready exists
		//if so a new uid is created
		let uidValue = createUid();
		//Constructs and pushes the the object
		var usedName = "";
		if (inputAmmount == 1) {
			usedName = inputname;
		} else {
			usedName = inputname + " " + (i + 1);
		}
		this.enemy = {
			type: "enemy",
			uid: uidValue,
			name: usedName,
			init: Math.floor((Math.random() * 20) + 1) + inputMod,
			hp: inputHp,
			damage: function(number) {
				this.hp = this.hp - number;
				console.log(this.name + ' took ' + number + ' damage');
			},
			heal: function(number) {
				this.hp = this.hp + number;
				console.log(this.name + ' healed for: ' + number);
			},
			initChange: function(number) {
				this.init = number;
			}
		}
		all.push(this.enemy)
	}
	//shows the new createt characters
	output();
	//closes the modal
	$("#enemyModal").modal("hide");
	//resets the form
	document.getElementById("enemyForm").reset();
	console.log(all);
}

//create a player character
function createPlayer() {
	var inputname = document.getElementById("namePlayer").value;
	var inputHp = Number(document.getElementById("hpPlayer").value);
	var inputInit = Number(document.getElementById("initativePlayer").value);


	//Creates uid for character and checks if the same uid allready exists
	//if so a new uid is created
	var uidValue = createUid();
	//Constructs the object
	this.player = {
		type: "player",
		uid: uidValue,
		name: inputname,
		init: inputInit,
		hp: inputHp,
		damage: function(number) {
			this.hp = this.hp - number;
			console.log(this.name + ' took ' + number + ' damage');
		},
		heal: function(number){
			this.hp = this.hp + number;
			console.log(this.name + ' healed for: ' + number);
		},
		initChange: function(number) {
			this.init = number;
		}
	}
	//Writes the chraracter to list
	document.getElementById("playerForm").reset();
	all.push(this.player);
	output();

	//close modal
	$("#playerModal").modal("hide");

	console.log(all);
}

//generates a sorted array of all characters
function sortInit() {
	allSorted = [].concat(all);
	allSorted.sort(function(a, b){return b.init - a.init});
	console.log("Sort: " + allSorted);
}

//searches for the position of a character based on uid
function search(input) {
	for (let i = 0; i < all.length; i++) {
		if (all[i].uid == input) {
			console.log(i);
			return i;
			break;
		}
	}
	return false;
}

//searches for the position of a character in sortet list based on uid
function searchSorted(input) {
	for (let i = 0; i < allSorted.length; i++) {
		if (allSorted[i].uid == input) {
			console.log(i);
			return i;
			break;
		}
	}
	return false;
}


//roll function
function roll(dice) {
	let input = Number(document.getElementById(dice).value);
	let dicenumber = Number(dice.substr(1));
	let output="";
	let list = [];

	//creates a list of rolled numbers
	for (let i = 0; i < input; i++){
		let value = Math.floor((Math.random() * dicenumber) + 1);
		list.push(value);
	}
	//sorts the list ascending
	list.sort(function(a, b){return a-b});
	//generates the html code and dispayes the output
	for (let i = 0; i < list.length; i++) {
		output = output + '<span>' + list[i] + '</span>  ';
	}
	document.getElementById("doutput").innerHTML = output;
}

//Advances to next turn and writes output
function nextTurn() {
	turn = turn + 1;
	output();
}
//Resets turn and writes output
function resetTurn() {
	turn = 0
	output();
}


//functions to edit characters
//delete
function characterDelete(name) {
	//goes a Turn back if deleted character is before current turn
	if (searchSorted(name) < currentTurn) {
		turn = turn - 1;
	}
	let x = search(name);
	if (typeof x == "number") {
		all.splice(x, 1);
	}
	//Kann eigentlich gelöscht werden
	document.getElementById("statChangeBody").reset();
	$("#changeStats").modal("hide");
	output();
}
//Damage UI
function createDamageModal(name) {
	let x = search(name);
	console.log("damge " + all[x].name);
	document.getElementById("statChangeHeader").innerHTML = "Damage: " + all[x].name;
	document.getElementById("statChangeBody").innerHTML = `Damage for:<br \>
		<input type="number" id="statHowMuch" placeholder="1" value="1" min="1"><br />`
	document.getElementById("statChangeBtn").innerHTML =`<button type="button" class="btn btn-success" onclick="characterDamage('` + name + `')">OK</button>`
	$("#changeStats").modal();
}
function characterDamage(name) {
	let x = search(name);
	let ammount = Number(document.getElementById("statHowMuch").value);
	all[x].damage(ammount);
	$("#changeStats").modal("hide");
	output();

}
//Heal UI
function createHealModal(name) {
	let x = search(name);
	console.log("damge " + all[x].name);
	document.getElementById("statChangeHeader").innerHTML = "Heal: " + all[x].name;
	document.getElementById("statChangeBody").innerHTML = `Heal for:<br \>
		<input type="number" id="statHowMuch" placeholder="1" value="1" min="1"><br />`
	document.getElementById("statChangeBtn").innerHTML =`<button type="button" class="btn btn-success" onclick="characterHeal('` + name + `')">OK</button>`
	$("#changeStats").modal();
}
function characterHeal(name) {
	let x = search(name);
	let ammount = Number(document.getElementById("statHowMuch").value);
	all[x].heal(ammount);
	$("#changeStats").modal("hide");
	output();
}

//int Change
function createIntChangeModal(name) {
	let x = search(name);
	console.log("damge " + all[x].name);
	document.getElementById("statChangeHeader").innerHTML = "Change Inintative: " + all[x].name;
	document.getElementById("statChangeBody").innerHTML = `New Initative Value:<br \>
		<input type="number" id="statHowMuch" placeholder="1" value="1" min="1"><br />`
	document.getElementById("statChangeBtn").innerHTML =`<button type="button" class="btn btn-success" onclick="characterIntchange('` + name + `')">OK</button>`
	$("#changeStats").modal();
}
function characterIntchange(name) {
	let x = search(name);
	let ammount = Number(document.getElementById("statHowMuch").value);
	all[x].initChange(ammount);
	$("#changeStats").modal("hide");
	output();
}

//changes the website
function output () {
	sortInit();
	var outputList = "";
	var outputEnemy = "";
	var outputPlayer = "";
	currentTurn = turn % allSorted.length;
	console.log("current Turn: " + currentTurn);

//Display turn list
	for (let i = 0; i < allSorted.length; i++) {
		if ( allSorted[i].hp <= 0 ) {
		}

		if (currentTurn == i) {
			if (allSorted[i].hp <= 0) {
				outputList = outputList + '<li class="list-group-item list-group-item-danger"><b>' + allSorted[i].name + '</b><br />Initative: ' + allSorted[i].init + '<br />Health: ' + allSorted[i].hp + '</li>';
			}
			else {
				outputList = outputList + '<li class="list-group-item active"><b>' + allSorted[i].name + '</b><br />Initative: ' + allSorted[i].init + '<br />Health: ' + allSorted[i].hp + '</li>';
			}
		}
		else {
			outputList = outputList + '<li class="list-group-item"><b>' + allSorted[i].name + '</b><br />Initative: ' + allSorted[i].init + '<br />Health: ' + allSorted[i].hp + '</li>';
		}
	}

	for (let i = 0; i < all.length; i++){
//Display enemys
		if (all[i].type == "enemy") {
			outputEnemy = outputEnemy + `
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">` + all[i].name + `</h4>
					<p class="card-text">
						HP:` + all[i].hp +`<br />
						Initative:` + all[i].init + `<br />
					</p>
					<div class="btn-group">
						<div class="btn-group">
							<button type="button" class="btn-primary" onclick="createDamageModal('` + all[i].uid + `')">Damage</button>
							<button type="button" class="btn-primary" onclick="createHealModal('` + all[i].uid + `')">Heal</button>
							<button type="button" class="btn-primary" onclick="createIntChangeModal('` + all[i].uid + `')">Change Int</button>
							<button type="button" class="btn-primary" onclick="characterDelete('` + all[i].uid + `')">Delete</button>
						</div>
					</div>
				</div>
			</div>`;
		}
	//Display players
		if (all[i].type == "player") {
			outputPlayer = outputPlayer + `
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">` + all[i].name + `</h4>
					<p class="card-text">
						HP:` + all[i].hp +`<br />
						Initative:` + all[i].init + `<br />
					</p>
					<div class="btn-group">
						<div class="btn-group">
						<button type="button" class="btn-primary" onclick="createDamageModal('` + all[i].uid + `')">Damage</button>
						<button type="button" class="btn-primary" onclick="createHealModal('` + all[i].uid + `')">Heal</button>
						<button type="button" class="btn-primary" onclick="createIntChangeModal('` + all[i].uid + `')">Change Int</button>
						<button type="button" class="btn-primary" onclick="characterDelete('` + all[i].uid + `')">Delete</button>
					</div>
					</div>
				</div>
			</div>`;
		}
	}
	document.getElementById("list").innerHTML = outputList;
	document.getElementById("enemys").innerHTML = outputEnemy;
	document.getElementById("player").innerHTML = outputPlayer;
}